import { Component } from "react";
import Button from "../Button/Button";
import IconCard from "../IconCard/IconCard";
import "./productCard.scss";

class ProductCard extends Component {
	checkTheCard() {
		let cardProds = JSON.parse(localStorage.getItem("card"));
		console.log(cardProds);
		let newCardProds = cardProds.map((el) => el);
		let result = newCardProds.includes(this.props.card.id);

		return result;
	}

	render() {
		const { card, onClick, iconClassName, favorites } = this.props;

		return (
			<div className="product-card" key={card.id} id={card.id}>
				<p>{card.name}</p>
				<div>
					<img
						src={card.routeImg}
						style={{ width: "auto", height: 300 }}
						alt={card.name}
					/>
				</div>
				<div>
					<p></p>
					<p>Price:{card.Price}</p>
					<p>Color: {card.color}</p>
				</div>
				<Button
					id={card.id}
					dataId={"first"}
					className={`button ${
						this.checkTheCard()
							? "button__added-to-card"
							: "button__add-to-card"
					}`}
					text={`${
						this.checkTheCard() ? "Добавлена в корзину" : "Добавить в корзину"
					}`}
					onClick={onClick}
				/>
				<IconCard id={card.id} className={iconClassName} onClick={favorites} />
			</div>
		);
	}
}

export default ProductCard;
