import { Component } from "react";
import "./Modal.scss";

class Modal extends Component {
	render() {
		const {
			header,
			closeButton,
			text,
			actions,
			backgroundColor,
			onClick,
			text2,
		} = this.props;
		return (
			<>
				<div className="modal-container" onClick={onClick}>
					<div
						className="modal-content"
						style={{ backgroundColor: backgroundColor }}
					>
						<div className="modal-content__header">
							{header}
							{closeButton && (
								<span
									className="modal-content__close-btn"
									onClick={onClick}
								></span>
							)}
						</div>
						<p className="modal-content__text">{text}</p>
						<span className="modal-content__span">{text2}</span>
						<div className="btn-container">{actions}</div>
					</div>
				</div>
			</>
		);
	}
}

export default Modal;
