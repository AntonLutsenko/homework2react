const modalParams = [
	{
		"data-Id": "first",
		header: "Добавить данный продукт в корзину?",
		text: `Данный телефон будет добавлен в корзину покупок`,
		backgroundColor: "rgb(15 193 74 / 90%)",
		closeButton: true,
		action1: "Ok",
		action2: "Cancel",
		classNameBtn1: "button btn-ok",
		classNameBtn2: "button btn-cancel",
	},
	{
		"data-Id": "second",
		header: "ong established fact that a reader will ?",
		text: "ince the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap i ",
		text2: `Where can I get some?`,
		backgroundColor: "rgba(76, 177, 76, 0.8)",
		closeButton: true,
		action1: "Ok",
		action2: "Cancel",
		classNameBtn1: "button btn-ok-second",
		classNameBtn2: "button btn-cancel-second",
	},
];

export default modalParams;
