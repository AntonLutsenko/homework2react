import { Component } from "react";
import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard";
import "./productList.scss";

class ProductList extends Component {
	render() {
		const { products, onClick, iconClassName, favorites } = this.props;

		const produktCards = products.map((element) => (
			<ProductCard
				key={element.id}
				card={element}
				onClick={onClick}
				favorites={favorites}
				iconClassName={iconClassName}
			/>
		));
		return <div className="product-cards">{produktCards}</div>;
	}
}

export default ProductList;
