import { Component } from "react";
import "./Button.scss";

class Button extends Component {
	constructor() {
		super();
		this.state = {};
	}

	render() {
		const { dataId, className, backgroundColor, onClick, text, id } =
			this.props;

		return (
			<button
				id={id}
				dataId={dataId}
				className={className}
				style={{ backgroundColor: backgroundColor }}
				onClick={onClick}
			>
				{text}
			</button>
		);
	}
}

export default Button;
