import "./Reset.css";
import { Component, useEffect } from "react";
import "./App.scss";

import Button from "./components/Button/Button";

import Header from "./components/Header/Header";
import axios from "axios";
import Modal from "./components/Modal/Modal";

import ProductList from "./components/ProductList/ProductList";
import modalParams from "./components/Modal/modalParams";

let modalDeclaration = {};

const [modalWindowDeclarations] = Object.values(modalParams);

class App extends Component {
	constructor() {
		super();
		this.state = {
			showModal: false,
			modalValue: {
				modalId: 0,
				modalHeader: "title",
				modalText: "text",
				backgroundColor: "",
				closeButton: false,
			},
			cards: [],
			displayState: "none",
			classWrap: "",
			iconClassName: "favor-icon",
			flag: "none",
			modalWindowDeclarations: modalWindowDeclarations,
		};
	}

	componentDidMount() {
		axios("/data.json").then((res) => {
			this.setState({ cards: res.data });
		});

		if (!localStorage.getItem("card")) localStorage.setItem("card", "[]");
		if (!localStorage.getItem("favorites"))
			localStorage.setItem("favorites", "[]");
	}

	adjustModal = (e) => {
		const dataId = e.target.dataId;
		const modalData = modalParams.find((element) => element.dataId === dataId);
		this.modalID = e.target.id;

		this.setState({
			showModal: true,
			modalValue: {
				dataId: modalData.dataId,
				header: modalData.header,
				text: modalData.text,
				text2: modalData.text2,
				backgroundColor: modalData.backgroundColor,
				closeButton: modalData.closeButton,
				action1: modalData.action1,
				action2: modalData.action2,
				classNameBtn1: modalData.classNameBtn1,
				classNameBtn2: modalData.classNameBtn2,
			},
		});
	};
	hideModal = () => {
		this.setState({ showModal: !this.state.showModal });
	};

	addToCart = (e) => {
		this.setState({
			displayState: "none",
		});
		let cartProds = JSON.parse(localStorage.getItem("card"));
		let newProds = cartProds.map((element) => element);
		newProds.push(this.modalID);
		localStorage.setItem("card", JSON.stringify(newProds));
	};

	addToFavorites = (e) => {
		let favoritesProducts = JSON.parse(localStorage.getItem("favorites"));
		let newProducts = favoritesProducts.map((element) => element);

		if (!newProducts.includes(e.target.parentElement.id)) {
			newProducts.push(e.target.parentElement.id);
			localStorage.setItem("favorites", JSON.stringify(newProducts));
			this.setState({
				flag: "add fav",
			});
		} else if (newProducts.includes(e.target.parentElement.id)) {
			let index = newProducts.indexOf(e.target.parentElement.id);
			newProducts.splice(index, 1);

			localStorage.setItem("favorites", JSON.stringify(newProducts));

			this.setState({
				flag: "remove fav",
			});
		}
	};

	render() {
		const { cards } = this.state;

		return (
			<>
				<Header
					actions={
						<Button
							dataId={"first"}
							className={"button"}
							text={"очистить"}
							backgroundColor={"rgb(255, 235, 205)"}
						/>
					}
				/>
				<ProductList
					products={cards}
					onClick={this.adjustModal}
					favorites={this.addToFavorites}
					iconClassName={this.state.iconClassName}
				/>

				{this.state.showModal && (
					<Modal
						onClick={this.hideModal}
						closeButton={this.state.modalValue.closeButton}
						header={this.state.modalValue.header}
						text={this.state.modalValue.text}
						backgroundColor={this.state.modalValue.backgroundColor}
						actions={
							<>
								<Button
									className={this.state.modalValue.classNameBtn1}
									text={this.state.modalValue.action1}
									onClick={this.addToCart}
								/>
								<Button
									className={this.state.modalValue.classNameBtn2}
									text={this.state.modalValue.action2}
								/>
							</>
						}
					/>
				)}
			</>
		);
	}
}
export default App;
